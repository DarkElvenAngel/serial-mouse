# Serial-Mouse

A simple program to allow old serial mice to be run on Linux.

This is primarily for my own use with a LOGITECH KIDZ Mouse from the early 90's.  The code is written to only support a mouse that uses the MICROSOFT 2 Button Mouse Protocol.

## USAGE  
Clone the repo and run **make**  
You have to run the program as *root* or with *sudo* this is because it creates a new input device named **TEST MOUSE** it can be accessed and used just like any device with the /dev/input/event*n* interface.  

The default device is **/dev/ttyUSB0** this can be change with the -d or --device= argument and select what ever device you mouse is connected to.  
The program doesn't write much to terminal by default but if you want no output the arguments -q, -s, --quiet, --silent Don't produce any output.  
Like wise if you want more output use -v, --verbose to produce verbose output.  
There is also -?, --help, --usage if you need help.  
and lastly -V, --version to print program version.  

It's important to note I haven't included a daemon into this version and you can just run it in the background with the & or run it from a terminal window.  

## CHANGE LOG  
0.0.1 initial release  
0.0.2 Code clean up, and optimized.  
0.0.3 Fix -Wanalyzer-unsafe-call-within-signal-handler

## KNOWN ISSUES  
None at this time.  

## PLANS  
I may develop this more if there is some call to do so I feel this is a very niche use case and it would be far easier to just get a new device.  

### Information sources I found useful  
[Mouse Protocol](https://linux.die.net/man/4/mouse)  
[Serial Programming](https://www.cmrr.umn.edu/~strupp/serial.html)
