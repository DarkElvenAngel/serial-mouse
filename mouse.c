// =====================================================================
// Title: Mouse
// By: Jeremy Brauer
// Date: May 2, 2020
// Version: 0.0.3
// Licence: <NONE> all rights reservered
// =====================================================================

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>
#include <argp.h>
#include <sys/types.h>
#include <linux/input.h>
#include <linux/uinput.h>

#include "serial.h"

#define die(str, args...) do { \
        perror(str); \
        handle_sigint(9); \
    } while(0)

/* =====================================================================
 * Packet Type
 * ================================================================== */
#pragma pack(1)
struct mouse_event
{
   union {
      struct {
         uint8_t  HiHorz_Motion: 2;
         uint8_t  HiVert_Motion: 2;
         uint8_t  RightButton : 1;
         uint8_t  LeftButton : 1;
         uint8_t  FirstByteFlag : 1;
      };  
      struct {
         uint8_t  Lo_Motion: 6;
         uint8_t  _zeroflag : 1;
      };
      uint8_t  raw;
   };
};
#pragma pack()


/* =====================================================================
 * Global Variables
 * ================================================================== */
struct mouse_event	   Buffer    	= {0};
t_serial 		         *rDev 		= NULL;
int                    	fd;
struct uinput_user_dev 	uidev;
struct input_event     	ev;

/* =====================================================================
 * Signal and Exit Handler
 * ================================================================== */
void handle_sigint(int sig) 
{ 
	fprintf(stderr, "QUITTING\n");
	serial_close(rDev); 
	ioctl(fd, UI_DEV_DESTROY);
    close(fd);
	exit(0);
} 
/// ====================================================================
/// Argumet Parsing
/// ====================================================================
const char *argp_program_version = "Serial Mouse v0.0.3";
const char *argp_program_bug_address =
	"<gitlab.com/darkelvenangel/serial-mouse.git>";

/* Program documentation. */
static char doc[] =
	"Serial Mouse - Make a serial mouse run under X";

/* A description of the arguments we accept. */
static char args_doc[] = "";

/* The options we understand. */
static struct argp_option options[] = {
	  {"verbose",  'v', 0,      0,  "Produce verbose output" },
	  {"quiet",    'q', 0,      0,  "Don't produce any output" },
	  {"silent",   's', 0,      OPTION_ALIAS },
	  {"device",   'd', "/dev/ttyUSB0", 0, "Input Device" },
	  { 0 }
};

/* Used by main to communicate with parse_opt. */
struct arguments
{
  int silent, verbose;
  char *serial_device;
};

/* Parse a single option. */
static error_t parse_opt (int key, char *arg, struct argp_state *state)
{
	/* Get the input argument from argp_parse, which we
     know is a pointer to our arguments structure. */
	struct arguments *arguments = state->input;

	switch (key)
    {
    case 'q': case 's':
      arguments->silent = 1;
      break;
    case 'v':
      arguments->verbose = 1;
      break;
    case 'd':
      arguments->serial_device = arg;
      break;
    case ARGP_KEY_END:
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
	return 0;
}

/* Our argp parser. */
static struct argp argp = { options, parse_opt, args_doc, doc };
struct arguments arguments;
/* =====================================================================
 * Main Loop
 * ================================================================== */
int main(int argc, char *argv[])
{
	/// Check Permisions
	uid_t uid=getuid();
	if (uid != 0) {
		fprintf (stderr,"You must run as root or with sudo!\n");
		return 1;
	}
	
	/// Arguements set to default values
	arguments.silent = 0;
	arguments.verbose = 0;
	arguments.serial_device = "/dev/ttyUSB0";


	/// Parse our arguments
	argp_parse (&argp, argc, argv, 0, 0, &arguments);
	
	
	/// Signal Handling
	signal(SIGINT, handle_sigint);
	
	/// Fire up serial
	rDev = serial_new();
	if(!arguments.silent) fprintf(stderr,"Open PORT %s\n", arguments.serial_device);
	if (serial_open(rDev, arguments.serial_device) != 0)
		die("error: Open Port");
	if (serial_setBaud(rDev, 1200) != 0)
		die("error: Set Baud");
	serial_setParity(rDev, "7N1");
	serial_setControl(rDev, 1, 1);
	
	/// Fire Up UINPUT
	fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK);
    if(fd < 0)
        die("error: open");

    if(ioctl(fd, UI_SET_EVBIT, EV_KEY) < 0)
        die("error: ioctl");
    if(ioctl(fd, UI_SET_KEYBIT, BTN_LEFT) < 0)
        die("error: ioctl");
	if(ioctl(fd, UI_SET_KEYBIT, BTN_RIGHT) < 0)
        die("error: ioctl");
    if(ioctl(fd, UI_SET_EVBIT, EV_REL) < 0)
        die("error: ioctl");
    if(ioctl(fd, UI_SET_RELBIT, REL_X) < 0)
        die("error: ioctl");
    if(ioctl(fd, UI_SET_RELBIT, REL_Y) < 0)
        die("error: ioctl");

    memset(&uidev, 0, sizeof(uidev));
    snprintf(uidev.name, UINPUT_MAX_NAME_SIZE, "TEST MOUSE");
    uidev.id.bustype = BUS_USB;
    uidev.id.vendor  = 0x1;
    uidev.id.product = 0x1;
    uidev.id.version = 1;

    if(write(fd, &uidev, sizeof(uidev)) < 0)
        die("error: write");

    if(ioctl(fd, UI_DEV_CREATE) < 0)
        die("error: ioctl");
	/// Main LOOP
	if(!arguments.silent)printf("Mouse initializing!\n");
	while (Buffer.raw != 'M')
	{
		serial_read(rDev, &Buffer,1);
		usleep(1000);
	}
	char count = 0;
	char btnL =0, btnR =0;
	signed char x = 0, y = 0;
	while (1)
	{
		int n = read(rDev->port_fd, &Buffer, 1);
		if (n)  // validate packets
		{
			switch (count)
			{
				case 0:
					if (!Buffer.FirstByteFlag ) n = 0; // Bad packet 1
					break;
				default:
					if (Buffer._zeroflag)  // Bad packet 2 or 3
					{  // throw out all data
						n = 0;
						x = 0;
						y = 0;
						btnL = 0;
						btnR = 0;
						count = 0;
					}
			}
		}
		if (n) // if packet is good process it
		{
			if(arguments.verbose)printf("%d: ",count);
			if(arguments.verbose)printf ("%02X\t" , Buffer.raw);
			switch (count++)
			{
            case 0:               
               btnL = Buffer.LeftButton;
               btnR = Buffer.RightButton;
               y += Buffer.HiVert_Motion << 6;
               x += Buffer.HiHorz_Motion << 6;
               break;
            case 1:
               x += Buffer.Lo_Motion;
               break;
            case 2: 
               y += Buffer.Lo_Motion;
				
               /// SEND MOUSE DATA
               memset(&ev, 0, sizeof(struct input_event));
               ev.type = EV_REL;
               ev.code = REL_X;
               ev.value = x;
               if(write(fd, &ev, sizeof(struct input_event)) < 0)
                  die("error: write");

               memset(&ev, 0, sizeof(struct input_event));
               ev.type = EV_REL;
               ev.code = REL_Y;
               ev.value = y;
               if(write(fd, &ev, sizeof(struct input_event)) < 0)
                  die("error: write");
               /// RIGHT BUTTON	
               memset(&ev, 0, sizeof(struct input_event));
               ev.type = EV_KEY;
               ev.code = BTN_RIGHT;
               ev.value = btnR;
               if(write(fd, &ev, sizeof(struct input_event)) < 0)
                  die("error: write");
               /// LEFT BUTTON
               memset(&ev, 0, sizeof(struct input_event));
               ev.type = EV_KEY;
               ev.code = BTN_LEFT;
               ev.value = btnL;
               if(write(fd, &ev, sizeof(struct input_event)) < 0)
                  die("error: write");

               memset(&ev, 0, sizeof(struct input_event));
               ev.type = EV_SYN;
               ev.code = 0;
               ev.value = 0;
               if(write(fd, &ev, sizeof(struct input_event)) < 0)
                  die("error: write");
               
               /// DUMP TO SCREEN
               if(arguments.verbose)printf (" Left: %d", btnL); 
               if(arguments.verbose)printf (" Right: %d", btnR); 
               if(arguments.verbose)printf (" X: %4d", x );
               if(arguments.verbose)printf (" Y: %4d", y);
               count = 0;
               x = 0;
               y = 0;
               if(arguments.verbose)putchar('\n');
			}
		}
		usleep(500);
	}
	return 0;
}
