## Very Simple Makefile 
CC 		=	gcc
RM		=	rm
BIN_NAME	= 	mouse
CFLAGS		=	-Wall
all:
	@echo -n "BUILD: $(BIN_NAME)  "
	@$(CC) serial.c mouse.c -o $(BIN_NAME) $(CFLAGS) && echo OK || { echo ERR ; false ; }
	@echo Complete

clean:
	$(RM) $(BIN_NAME)
